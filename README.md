Вариант выполнения [Тестового задания](https://github.com/RoboFinance/test-assignments/releases/tag/20201008) для ROBOFINANCE.

# Деплой приложения
Для корректной работы требуются установленный [Docker CE](https://docs.docker.com/engine/install/ubuntu/) и [docker-compose](https://docs.docker.com/compose/install/).

```
$ mkdir /path-to-your-project
$ cd /path-to-your-project
$ git clone git@gitlab.com:futualien/robofinance-test.git .
```

## Перед началом работы
Необходимо скопировать .env.example для докера и для laravel
```bash
# docker
$ cp .env.example .env
$ nano .env

# laravel
$ cd www
$ cp .env.example .env
$ nano .env
```
В зависимости от имени директории и поднятого для базы данных контейнера необходимо указать DB_HOST (например DB_HOST=test-deploy_db_1)

## Докер
В зависимости от установки докера могут потребоваться root-права для запуска docker-compose.
```bash
$ cd /path-to-your-project
$ docker-compose build
$ docker-compose up -d
$ docker-compose exec php bash

## далее внутри контейнера
$ composer install
$ php artisan key:generate
$ chmod -R a+w storage/
$ php artisan migrate --seed

# запуск тестов (внутри контейнера)
$ phpunit
```

При правках frontend:
```bash
$ docker-compose exec php bash
## далее внутри контейнера, unsafe-perm т.к. запускается от рута внутри контейнера
$ npm install --unsafe-perm
$ npm run prod
```

# После деплоя

Открываем в браузере http://localhost:8080 (Если порт был изменен в .env докера, то указываем его)

Пользователь c положительным балансом: 
```user@app.com```

Пароль (у всех пользователей одинаковый)
```password```

Можно взять емаил любого пользователя из списка и залогиниться под ним.
