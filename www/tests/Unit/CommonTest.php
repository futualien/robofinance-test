<?php

namespace Tests\Unit;

use App\Models\DelayedPayment;
use App\Models\User;
use Tests\TestCase;
use PaymentService;

class CommonTest extends TestCase
{
    /**
     * @return array
     */
    public function testCreateUsers(): array
    {
        $users = User::factory(2)->create();

        $this->assertEquals(2, $users->count());

        $this->assertSame(1, $users->first()->increment('balance', 1000));

        $response = $this->actingAs($users->first())
            ->get('/api/v1/me');

        $response->assertStatus(200)
            ->assertJson(
                [
                    "user" => $users->first()->toArray()
                ]
            );

        return [$users->first(), $users->last()];
    }

    /**
     * A main test
     * @depends testCreateUsers
     * @return void
     */
    public function testPaymentController(array $users)
    {
        [$user1, $user2] = $users;

        $this->actingAs($user1)
            ->postJson('/api/v1/payment', [])
            ->assertStatus(422);

        // amount more than have
        $this->actingAs($user1)
            ->postJson(
                '/api/v1/payment',
                [
                    'amount' => 999999,
                    'toUser' => $user2->id,
                    'datetime' => now()
                ]
            )
            ->assertStatus(422);

        // amount below zero
        $this->actingAs($user1)
            ->postJson(
                '/api/v1/payment',
                [
                    'amount' => -100,
                    'toUser' => $user2->id,
                    'datetime' => now()
                ]
            )
            ->assertStatus(422);

        // user doesnt exist
        $this->actingAs($user1)
            ->postJson(
                '/api/v1/payment',
                [
                    'amount' => 100,
                    'toUser' => -1,
                    'datetime' => now()
                ]
            )
            ->assertStatus(422);

        $this->actingAs($user1)
            ->postJson(
                '/api/v1/payment',
                [
                    'amount' => 100,
                    'toUser' => $user2->id,
                    'datetime' => now()->addHour()
                ]
            )
            ->assertStatus(200)
            ->assertJson(
                [
                    'error' => 0,
                    'transaction' => $user1->lastTransaction()->first()->toArray(),
                ]
            );

        return $users;
    }

    /**
     * @param array $users
     * @depends testPaymentController
     * @return void
     */
    public function testTransactions(array $users)
    {
        [$user1, $user2] = $users;

        $this->assertSame(0.0, $user2->balance);

        $response = $this->actingAs($user1)
            ->postJson(
                '/api/v1/payment',
                [
                    'amount' => 100,
                    'toUser' => $user2->id,
                    'datetime' => now()->subMinute()
                ]
            );

        $response->assertStatus(200)
            ->assertJson(
                [
                    'error' => 0,
                    'transaction' => $user1->lastTransaction()->first()->toArray(),
                ]
            );
        $user2->refresh();
        $this->assertSame(100.0, $user2->balance);


        $response = $this->actingAs($user1)
            ->postJson(
                '/api/v1/payment',
                [
                    'amount' => 200,
                    'toUser' => $user2->id,
                    'datetime' => now()->addMinutes(15)
                ]
            );

        $lastTransaction = $user1->lastTransaction()->first();
        $response->assertStatus(200)
            ->assertJson(
                [
                    'error' => 0,
                    'transaction' => $lastTransaction->toArray(),
                ]
            );
        $user2->refresh();
        $this->assertSame(100.0, $user2->balance);

        $delayedPayment = DelayedPayment::where('finish_at', '<=', now()->addMinutes(16))
            ->where('transaction_id', $lastTransaction->id)
            ->with('transaction')
            ->first();

        $this->assertNotNull($delayedPayment);

        (new PaymentService())->process($delayedPayment->transaction);
        // important
        $delayedPayment->delete();

        $user2->refresh();
        $this->assertSame(300.0, $user2->balance);
    }


    /**
     * @depends testPaymentController
     * @return void
     */
    public function testDeleteUsers(array $users)
    {
        [$user1, $user2] = $users;
        collect($users)->each(
            function ($user) {
                $user->delete();
            }
        );

        $this->assertDatabaseMissing(User::class, ['id' => $user1['id']]);
        $this->assertDatabaseMissing(User::class, ['id' => $user2['id']]);
    }
}
