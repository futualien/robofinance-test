<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserBalance;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::factory(20)->create();

        $main = \App\Models\User::find(1);
        $main->email = 'user@app.com';
        $main->balance = 100000;
        $main->update();
    }
}
