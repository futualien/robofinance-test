<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table): void {
            $table->decimal('balance', 8, 2);
        });

        Schema::create('transactions', function(Blueprint $table): void {
            $table->id('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('to_user_id');
            $table->decimal('start_amount', 8,2);
            $table->decimal('end_amount', 8,2);
            $table->decimal('amount', 8,2);
            $table->enum('status', \App\Models\Transaction::STATUSES);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('to_user_id')->references('id')->on('users')
                ->onDelete('cascade');
        });

        Schema::create('delayed_payments', function(Blueprint $table): void {
            $table->id('id');
            $table->unsignedInteger('transaction_id');
            $table->dateTime('finish_at');

            $table->foreign('transaction_id')->references('id')->on('transactions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->dropColumn('balance');
        });
        Schema::dropIfExists('user_balances');
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('delayed_payments');
    }
}
