<?php

namespace App\Observers;

use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class TransactionObserver
{
    /**
     * Handle the transaction "created" event.
     *
     * @param  Transaction $transaction
     * @return void
     */
    public function created(Transaction $transaction): void
    {
        $transaction->user->decrement('balance', $transaction->amount);
    }

    /**
     * Handle the transaction "updated" event.
     *
     * @param  Transaction $transaction
     * @return void
     */
    public function updated(Transaction $transaction): void
    {
        if ($transaction->getOriginal('status') !== $transaction->status) {
            switch ($transaction->status) {
                case Transaction::STATUS_CANCEL:
                    $transaction->user->increment('balance', $transaction->amount);
                    break;
                case Transaction::STATUS_PROGRESS:
                    DB::transaction(function () use ($transaction) {
                        // и так без событий
                        $transaction->update(['status' => Transaction::STATUS_DONE]);

                        // Транзакция пополнения для пользователя
                        Transaction::withoutEvents(function () use ($transaction) {
                            $reverseTransaction = $transaction->replicate();
                            $reverseTransaction->user_id = $transaction->to_user_id;
                            $reverseTransaction->to_user_id = $transaction->user_id;
                            //refresh relations
                            $reverseTransaction->load(['user', 'toUser']);
                            $reverseTransaction->start_amount = $reverseTransaction->user->balance;
                            $reverseTransaction->end_amount = $reverseTransaction->user->balance + $reverseTransaction->amount;
                            $reverseTransaction->save();

                            $transaction->toUser->increment('balance', $transaction->amount);
                        });

                        return true;
                    });
                break;

            }
        }
    }
}
