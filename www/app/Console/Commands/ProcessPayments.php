<?php

namespace App\Console\Commands;

use App\Models\DelayedPayment;
use Illuminate\Console\Command;
use PaymentService;
use Log;

class ProcessPayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'process-payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process delayed payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(PaymentService $paymentService)
    {
        Log::debug('Start handle delayed payments');

        DelayedPayment::where('finish_at', '<=', now())
            ->with('transaction')
            ->each(function($delayedPayment) use ($paymentService) {
                Log::debug('Handling the ', $delayedPayment->toArray());
                $paymentService->process($delayedPayment->transaction);
                $delayedPayment->delete();
            });

        return 0;
    }
}
