<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    const STATUS_NEW = 'new';
    const STATUS_HOLD = 'hold';
    const STATUS_PROGRESS = 'progress';
    const STATUS_DONE = 'done';
    const STATUS_CANCEL = 'cancel';

    const STATUSES = [
        self::STATUS_NEW,
        self::STATUS_HOLD,
        self::STATUS_PROGRESS,
        self::STATUS_DONE,
        self::STATUS_CANCEL,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'to_user_id',
        'start_amount',
        'end_amount',
        'amount',
        'status'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_amount' => 'float',
        'end_amount' => 'float',
        'amount' => 'float'
    ];

    public $appends = [
        'direction'
    ];

    public function delayedPayment()
    {
        return $this->hasOne(DelayedPayment::class);
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function toUser()
    {
        return $this->hasOne(User::class, 'id', 'to_user_id');
    }

    public function getDirectionAttribute()
    {
        return $this->start_amount > $this->end_amount;
    }
}
