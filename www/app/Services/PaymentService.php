<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\DelayedPayment;
use App\Models\Transaction;
use Auth;

class PaymentService
{

    public function create(array $data): Transaction
    {
        $transaction = Transaction::create(
            [
                'user_id' => Auth::user()->id,
                'to_user_id' => $data['toUser'],
                'start_amount' => Auth::user()->balance,
                'end_amount' => Auth::user()->balance - $data['amount'],
                'amount' => $data['amount'],
                'status' => Transaction::STATUS_NEW
            ]
        );

        $date = Carbon::parse($data['datetime']);

        if ($date->gt(Carbon::now())) {
            $this->schedule($transaction, $date);
        } else {
            $this->process($transaction);
        }

        return $transaction;
    }

    private function schedule(Transaction $transaction, Carbon $date): DelayedPayment
    {
        $transaction->update(['status' => Transaction::STATUS_HOLD]);

        return DelayedPayment::create(
            [
                'transaction_id' => $transaction->id,
                'finish_at' => $date
            ]
        );
    }

    public function process(Transaction $transaction): Transaction
    {
        $transaction->status = Transaction::STATUS_PROGRESS;
        $transaction->save();
        return $transaction;
    }
}
