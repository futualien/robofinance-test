<?php

namespace App\Http\Requests;

use App\Rules\PaymentAmount;
use Illuminate\Foundation\Http\FormRequest;

class NewPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => ['required', new PaymentAmount, 'numeric', 'min:0.01'],
            'toUser' => ['required', 'integer', 'exists:users,id'],
            'datetime' => ['required']
        ];
    }
}
