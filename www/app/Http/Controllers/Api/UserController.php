<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;

class UserController extends Controller
{
    public function me()
    {
        return response()->json(
            [
                'user' => User::with('lastTransaction', 'lastTransaction.user', 'lastTransaction.toUser')
                    ->find(Auth::user()->id)
            ]
        );
    }

    public function show(int $id)
    {
        return response()->json(
            [
                'user' => User::with('lastTransaction', 'lastTransaction.user', 'lastTransaction.toUser')
                    ->find($id)
            ]
        );
    }

    public function index()
    {
        // TODO: the paginator is needed here
        return response()->json(
            [
                'users' => User::where('id', '!=', Auth::user()->id)
                    ->orderBy('id', 'asc')
                    ->get()
            ]
        );
    }

}
