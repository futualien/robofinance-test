<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewPaymentRequest;
use App\Models\Transaction;
use PaymentService;

class PaymentController extends Controller
{
    public function store(NewPaymentRequest $request, PaymentService $paymentService)
    {
        $transaction = $paymentService->create($request->all());
        $transaction->load('user', 'toUser');

        return response()->json(
            [
                'error' => 0,
                'transaction' => $transaction,
                'message' => 'Перевод успешно ' . ($transaction->status === Transaction::STATUS_HOLD ? 'запланирован' : 'выполнен') . '.'
            ]
        );
    }
}
