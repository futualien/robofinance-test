<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Auth;

class PaymentAmount implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return floor(Auth::user()->balance * 100) >= floor($value * 100);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Недостаточно средств для перевода.';
    }
}
