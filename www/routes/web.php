<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('main');
})->middleware('auth');


Route::group(['prefix' => 'api/v1', 'as' => 'api.v1.', 'middleware' => ['web', 'auth'], 'namespace' => 'Api\\'], function () {
    Route::get('me', 'UserController@me');
    Route::resource('user', 'UserController')->only(['index', 'show']);
    Route::resource('payment', 'PaymentController')->only(['index', 'store', 'show', 'delete']);
});

Auth::routes();
