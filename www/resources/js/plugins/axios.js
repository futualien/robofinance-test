import axios from 'axios';

axios.defaults.baseURL = process.env.baseURL || '/api/v1/';

export default {
    install: function(Vue) {
        Object.defineProperty(Vue.prototype, '$axios', { value: axios });
    }
}
